edgetx-sdcard-builder
=====================

edgetx-sdcard-builder aims to make creating and updating an EdgeTX SD card with all the additional components as simple as possible, nominally a single command.

It is highly recommended that this not be used directly on the SD card, but instead used to create a directory in a user's home directory or a temporary directory, then copy the contents over to the SD card.

Currently, this targets Unix-style command line environments, such as Linux and macOS, that includes relatively common tools, like make, bsdtar, and curl.

## Usage
1. Back up the contents of your SD card.
2. Really, back up the contents of your SD card.
3. Clone this project to a directory other than your SD card.
4. Look at the variables in the Makefile, and update as appropriate for your radio.
5. Run `make`. This will produce a directory named after the selected parameters starting with "sdcard-", such as "sdcard-edgetx-horus-2.5.0".
6. Copy the contents of the "sdcard-" directory (not the directory itself) over to the root of your SD card, overwriting any conflicts.

## Contributions
Constructive contributions, including non-technical bug reports and feature requests, to this project are welcomed. Issues and merge requests are the recommended ways to engage.
