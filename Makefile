###### Start of user-editable variables ######

# Find EDGETX_VERSION here: https://github.com/EdgeTX/edgetx/releases
EDGETX_VERSION := 2.9.0
#EDGETX_FW_VARIANT := tx16s
#EDGETX_FW_VARIANT := x9dp2019
EDGETX_FW_VARIANT := boxer
# Find EDGETX_SDCARD_VERSION here: https://github.com/EdgeTX/edgetx-sdcard/releases
EDGETX_SDCARD_VERSION := 2.9.0
EDGETX_SDCARD_VARIANT := bw128x64
#EDGETX_SDCARD_VARIANT := c480x272
#EDGETX_SDCARD_VARIANT := bw212x64
# Find EDGETX_SOUNDS_VERSION here: https://github.com/EdgeTX/edgetx-sdcard-sounds/releases
EDGETX_SOUNDS_VERSION := $(EDGETX_SDCARD_VERSION)
EDGETX_SOUNDS_VARIANT := en_us-sara
EDGETX_SOUNDS_DESTINATION := en

# Find MPM_VERSION here: https://github.com/pascallanger/DIY-Multiprotocol-TX-Module/releases/
MPM_VERSION := 1.3.3.20
MPM_VARIANT := stm
MPM_PROTOCOL := serial
MPM_CHAN := aetr

# Find BETAFLIGHT_LUA_VERSION here: https://github.com/betaflight/betaflight-tx-lua-scripts/releases
BETAFLIGHT_LUA_VERSION := 1.7.0

# Find ELRS_VERSION here: https://github.com/ExpressLRS/ExpressLRS/releases
ELRS_VERSION := 3.3.0
# Find ELRS_VERSION here: https://github.com/ExpressLRS/ElrsTelemWidget/releases
ELRS_WIDGET_VERSION := 6

# TX16S has a screen resolution of 480×272 with a ratio of 30:17.
# Closest standard resolution is 1920x1080.
#BACKGROUND_URL := https://digitalblasphemy.com/graphics/HDfree/acceleratorHDfree.jpg
#BACKGROUND_URL := https://digitalblasphemy.com/graphics/HDfree/arcana2HDfree.jpg
#BACKGROUND_URL := https://digitalblasphemy.com/graphics/HDfree/capricornHDfree.jpg
#BACKGROUND_URL := https://digitalblasphemy.com/graphics/HDfree/chameleaHDfree.jpg
#BACKGROUND_URL := https://digitalblasphemy.com/graphics/HDfree/circularlogicHDfree.jpg
#BACKGROUND_URL := https://digitalblasphemy.com/graphics/HDfree/cronusrising2k141HDfree.jpg
#BACKGROUND_URL := https://digitalblasphemy.com/graphics/HDfree/dispersionHDfree.jpg
#BACKGROUND_URL := https://digitalblasphemy.com/graphics/HDfree/epistemeHDfree.jpg
#BACKGROUND_URL := https://digitalblasphemy.com/graphics/HDfree/fluorescence6HDfree.jpg
#BACKGROUND_URL := https://digitalblasphemy.com/graphics/HDfree/gardener2k121HDfree.jpg
BACKGROUND_URL := https://digitalblasphemy.com/graphics/HDfree/haiku2k72HDfree.jpg
#BACKGROUND_URL := https://digitalblasphemy.com/graphics/HDfree/harbinger2k91HDfree.jpg
#BACKGROUND_URL := https://digitalblasphemy.com/graphics/HDfree/idyllHDfree.jpg
#BACKGROUND_URL := https://digitalblasphemy.com/graphics/HDfree/metallurgy1HDfree.jpg
#BACKGROUND_URL := https://digitalblasphemy.com/graphics/HDfree/moonshadow1HDfree.jpg
#BACKGROUND_URL := https://digitalblasphemy.com/graphics/HDfree/portals1HDfree.jpg
#BACKGROUND_URL := https://digitalblasphemy.com/graphics/HDfree/rivenHDfree.jpg
#BACKGROUND_URL := https://digitalblasphemy.com/graphics/HDfree/sierraautumn1HDfree.jpg
#BACKGROUND_URL := https://digitalblasphemy.com/graphics/HDfree/skysong2HDfree.jpg
#BACKGROUND_URL := https://digitalblasphemy.com/graphics/HDfree/skysongyuletideHDfree.jpg
#BACKGROUND_URL := https://digitalblasphemy.com/graphics/HDfree/snowglobefluorescence1HDfree.jpg

###### End of user-editable variables ######

# Download file names
EDGETX_FW_ARCHIVE := edgetx-firmware-v$(EDGETX_VERSION).zip
URL_EDGETX_FW_ARCHIVE := https://github.com/EdgeTX/edgetx/releases/download/v$(EDGETX_VERSION)/$(EDGETX_FW_ARCHIVE)
DL_EDGETX_FW_ARCHIVE := downloads/$(EDGETX_FW_ARCHIVE)

# TODO: Add companion support (maybe?)
#EDGETX_COMPANION_ARCHIVE := https://github.com/EdgeTX/edgetx/releases/download/v2.5.0/edgetx-companion-osx-v2.5.0.zip

EDGETX_SDCARD_ARCHIVE := $(EDGETX_SDCARD_VARIANT).zip
URL_EDGETX_SDCARD_ARCHIVE := https://github.com/EdgeTX/edgetx-sdcard/releases/download/v$(EDGETX_SDCARD_VERSION)/$(EDGETX_SDCARD_ARCHIVE)
DL_EDGETX_SDCARD_ARCHIVE := downloads/edgetx-sdcard-$(EDGETX_SDCARD_VERSION)-$(EDGETX_SDCARD_VARIANT).zip

EDGETX_SOUNDS_ARCHIVE := edgetx-sdcard-sounds-$(EDGETX_SOUNDS_VARIANT)-$(EDGETX_SOUNDS_VERSION).zip
URL_EDGETX_SOUNDS_ARCHIVE := https://github.com/EdgeTX/edgetx-sdcard-sounds/releases/download/v$(EDGETX_SOUNDS_VERSION)/$(EDGETX_SOUNDS_ARCHIVE)
#URL_EDGETX_SOUNDS_ARCHIVE := https://github.com/EdgeTX/edgetx-sdcard-sounds/releases/download/latest/$(EDGETX_SOUNDS_ARCHIVE)
DL_EDGETX_SOUNDS_ARCHIVE := downloads/$(EDGETX_SOUNDS_ARCHIVE)

AGENT_LITE_ARCHIVE := tbs-agent-lite.zip
DL_AGENT_LITE_ARCHIVE := downloads/$(AGENT_LITE_ARCHIVE)

MPM_SCRIPTS_ARCHIVE := MultiLuaScripts-v$(MPM_VERSION).zip
DL_MPM_SCRIPTS_ARCHIVE := downloads/$(MPM_SCRIPTS_ARCHIVE)

MPM_FIRMWARE_BIN := mm-$(MPM_VARIANT)-$(MPM_PROTOCOL)-$(MPM_CHAN)-v$(MPM_VERSION).bin
DL_MPM_FIRMWARE_BIN := downloads/$(MPM_FIRMWARE_BIN)

BETAFLIGHT_LUA_ARCHIVE := betaflight-tx-lua-scripts_$(BETAFLIGHT_LUA_VERSION).zip
DL_BETAFLIGHT_LUA_ARCHIVE := downloads/$(BETAFLIGHT_LUA_ARCHIVE)

#URL_ELRS_LUA := https://raw.githubusercontent.com/ExpressLRS/ExpressLRS/$(ELRS_VERSION)/src/lua/elrsV2.lua
URL_ELRS_LUA := https://raw.githubusercontent.com/ExpressLRS/ExpressLRS/$(ELRS_VERSION)/src/lua/elrsV3.lua
DL_ELRS_LUA := downloads/elrsV3-$(ELRS_VERSION).lua

URL_ELRS_WIDGET_LUA := https://github.com/ExpressLRS/ElrsTelemWidget/releases/download/v$(ELRS_WIDGET_VERSION)/ElrsTelemWidget-v$(ELRS_WIDGET_VERSION).zip
DL_ELRS_WIDGET_LUA := downloads/ElrsTelemWidget-v$(ELRS_WIDGET_VERSION).zip

AMBER_ARCHIVE := Amber22.7z
DL_AMBER_ARCHIVE := downloads/$(AMBER_ARCHIVE)

IMAGES := $(wildcard images/*)

DL_BACKGROUND := downloads/backgrounds/$(notdir $(BACKGROUND_URL))
RESIZED_BACKGROUND := $(DL_BACKGROUND:.jpg=_resized.jpg)

# Outputs
EDGETX_SDCARD_DIR := sdcard-$(EDGETX_SDCARD_VARIANT)-$(EDGETX_SDCARD_VERSION)

# TOOLS
CURL := curl --location --fail

default: $(EDGETX_SDCARD_DIR)
.PHONY: default

EDGETX_SDCARD_DEPS = $(DL_EDGETX_SDCARD_ARCHIVE)
$(DL_EDGETX_SDCARD_ARCHIVE):
	$(CURL) -o $@ $(URL_EDGETX_SDCARD_ARCHIVE)

EDGETX_SDCARD_DEPS += $(DL_EDGETX_FW_ARCHIVE)
$(DL_EDGETX_FW_ARCHIVE):
	$(CURL) -o $@ $(URL_EDGETX_FW_ARCHIVE)

EDGETX_SDCARD_DEPS += $(DL_EDGETX_SOUNDS_ARCHIVE)
$(DL_EDGETX_SOUNDS_ARCHIVE):
	$(CURL) -o $@ $(URL_EDGETX_SOUNDS_ARCHIVE)

EDGETX_SDCARD_DEPS += $(DL_MPM_SCRIPTS_ARCHIVE)
$(DL_MPM_SCRIPTS_ARCHIVE):
	$(CURL) -o $@ https://github.com/pascallanger/DIY-Multiprotocol-TX-Module/releases/download/v$(MPM_VERSION)/MultiLuaScripts.zip

EDGETX_SDCARD_DEPS += $(DL_MPM_FIRMWARE_BIN)
$(DL_MPM_FIRMWARE_BIN):
	$(CURL) -o $@  https://github.com/pascallanger/DIY-Multiprotocol-TX-Module/releases/download/v$(MPM_VERSION)/mm-$(MPM_VARIANT)-$(MPM_PROTOCOL)-$(MPM_CHAN)-v$(MPM_VERSION).bin

# Does this have a version?
EDGETX_SDCARD_DEPS += $(DL_AGENT_LITE_ARCHIVE)
$(DL_AGENT_LITE_ARCHIVE):
	$(CURL) -o $@ https://www.team-blacksheep.com/tbs-agent-lite.zip

EDGETX_SDCARD_DEPS += $(DL_BETAFLIGHT_LUA_ARCHIVE)
$(DL_BETAFLIGHT_LUA_ARCHIVE):
	$(CURL) -o $@ https://github.com/betaflight/betaflight-tx-lua-scripts/releases/download/$(BETAFLIGHT_LUA_VERSION)/betaflight-tx-lua-scripts_$(BETAFLIGHT_LUA_VERSION).zip

EDGETX_SDCARD_DEPS += $(DL_ELRS_LUA)
$(DL_ELRS_LUA):
	$(CURL) -o $@ $(URL_ELRS_LUA)

EDGETX_SDCARD_DEPS += $(DL_ELRS_WIDGET_LUA)
$(DL_ELRS_WIDGET_LUA):
	$(CURL) -o $@ $(URL_ELRS_WIDGET_LUA)

$(DL_AMBER_ARCHIVE):
	$(CURL) -o $@ https://hmvc.eu/$(AMBER_ARCHIVE)

EDGETX_SDCARD_DEPS += $(RESIZED_BACKGROUND)
$(RESIZED_BACKGROUND):
	mkdir -pv downloads/backgrounds
	$(CURL) -o $(DL_BACKGROUND) $(BACKGROUND_URL)
	convert $(DL_BACKGROUND) -scale 480x272 $(RESIZED_BACKGROUND)

sdcard-%: $(EDGETX_SDCARD_DEPS)
	install -d $@
	# Building SD card directory
	bsdtar -C $@ -xf $(DL_EDGETX_SDCARD_ARCHIVE)
	# Add EdgeTX Firmware
	bsdtar -C ${@}/FIRMWARE -xf $(DL_EDGETX_FW_ARCHIVE) '$(EDGETX_FW_VARIANT)-???????.bin'
	mv ${@}/FIRMWARE/$(EDGETX_FW_VARIANT)-*.bin ${@}/FIRMWARE/edgetx-$(EDGETX_FW_VARIANT)-$(EDGETX_VERSION).bin
	# Add EdgeTX SD Card Sounds
	bsdtar -C $@ -xf $(DL_EDGETX_SOUNDS_ARCHIVE)
	#test $(EDGETX_SOUNDS_VARIANT) != $(EDGETX_SOUNDS_DESTINATION) && mv ${@}/SOUNDS/$(EDGETX_SOUNDS_VARIANT) ${@}/SOUNDS/$(EDGETX_SOUNDS_DESTINATION)
	# Add Multiprotocol Module firmware
	install $(DL_MPM_FIRMWARE_BIN) $@/FIRMWARE
	bsdtar -C $@ -xf $(DL_MPM_SCRIPTS_ARCHIVE)
	# Add TBS Agent Lite
	bsdtar -C ${@}/SCRIPTS/TOOLS -xf $(DL_AGENT_LITE_ARCHIVE)
	# Add Betaflight Lua
	bsdtar -C $@ --strip-components=1 -xf $(DL_BETAFLIGHT_LUA_ARCHIVE)
	# Add ExpressLRS Lua
	install $(DL_ELRS_LUA) ${@}/SCRIPTS/TOOLS
	# Add ExpressLRS Widget
	bsdtar -C $@ -xf $(DL_ELRS_WIDGET_LUA)
	# Add images
	rm -rf ${@}/IMAGES/
	install -d ${@}/IMAGES/
	$(foreach img,$(IMAGES),install $(img) ${@}/IMAGES/$(notdir $(img));)
	# Add background
	install -d ${@}/THEMES/EdgeTX
	install $(RESIZED_BACKGROUND) ${@}/THEMES/EdgeTX/background.png
	# Finished
	@echo Finished building $@

amber-sounds: $(EDGETX_SDCARD_DIR) $(DL_AMBER_ARCHIVE)
	rm -rf $(EDGETX_SDCARD_DIR)/SOUNDS/en
	7z -y -o$(EDGETX_SDCARD_DIR) x $(DL_AMBER_ARCHIVE)

clean:
	rm -rf sdcard-*

dist-clean: clean
	rm -r downloads/*
